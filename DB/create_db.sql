﻿/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     12.05.2013 14:03:53                          */
/*==============================================================*/


drop index ElementLinkage_unique;

drop table ElementLinkage;

drop index Linkage_unique;

drop table Linkage;

drop index Material_unique;

drop table Material;

drop index MaterialType_unique;

drop table MaterialType;

drop index Part_unique;

drop table Part;

drop index Person_unique;

drop table Person;

drop index Project_unique;

drop table Project;

drop index ProjectState_unique;

drop table ProjectState;

drop index SettlementUnit_unique;

drop table SettlementUnit;

drop index TrimmingType_unique;

drop table TrimmingType;

drop index Wrapping_unique;

drop table Wrapping;

drop index WrappingPart_unique;

drop table WrappingPart;

drop domain Code;

drop domain Comment;

drop domain Cost;

drop domain Date;

drop domain Dimension;

drop domain Flag;

drop domain Id_ElementLinkage;

drop domain Id_Linkage;

drop domain Id_Material;

drop domain Id_MaterialType;

drop domain Id_Part;

drop domain Id_Person;

drop domain Id_Project;

drop domain Id_ProjectState;

drop domain Id_SettlementUnit;

drop domain Id_TrimmingType;

drop domain Id_Wrapping;

drop domain Id_WrappingPart;

drop domain LargeName;

drop domain No;

drop domain ShortName;

drop domain Weight;

drop sequence Id_ElementLinkage_seq;

drop sequence Id_Linkage_seq;

drop sequence Id_MaterialType_seq;

drop sequence Id_Material_seq;

drop sequence Id_Part_seq;

drop sequence Id_Person_seq;

drop sequence Id_ProjectState_seq;

drop sequence Id_Project_seq;

drop sequence Id_SettlementUnit_seq;

drop sequence Id_TrimmingType_seq;

drop sequence Id_WrappingPart_seq;

drop sequence Id_Wrapping_seq;

create sequence Id_ElementLinkage_seq;

create sequence Id_Linkage_seq;

create sequence Id_MaterialType_seq;

create sequence Id_Material_seq;

create sequence Id_Part_seq;

create sequence Id_Person_seq;

create sequence Id_ProjectState_seq;

create sequence Id_Project_seq;

create sequence Id_SettlementUnit_seq;

create sequence Id_TrimmingType_seq;

create sequence Id_WrappingPart_seq;

create sequence Id_Wrapping_seq;

/*==============================================================*/
/* Domain: Code                                                 */
/*==============================================================*/
create domain Code as varchar(200);

comment on domain Code is
'Код';

/*==============================================================*/
/* Domain: Comment                                              */
/*==============================================================*/
create domain Comment as text;

comment on domain Comment is
'Комментарий';

/*==============================================================*/
/* Domain: Cost                                                 */
/*==============================================================*/
create domain Cost as numeric(12,2);

comment on domain Cost is
'Стоимость за единицу измерения';

/*==============================================================*/
/* Domain: Date                                                 */
/*==============================================================*/
create domain Date as date;

/*==============================================================*/
/* Domain: Dimension                                            */
/*==============================================================*/
create domain Dimension as numeric(12);

comment on domain Dimension is
'Размер в миллиметрах';

/*==============================================================*/
/* Domain: Flag                                                 */
/*==============================================================*/
create domain Flag as numeric(1);

/*==============================================================*/
/* Domain: Id_ElementLinkage                                    */
/*==============================================================*/
create domain Id_ElementLinkage as numeric(15);

comment on domain Id_ElementLinkage is
'УИД элемента сборки';

/*==============================================================*/
/* Domain: Id_Linkage                                           */
/*==============================================================*/
create domain Id_Linkage as numeric(15);

comment on domain Id_Linkage is
'УИД Сборки';

/*==============================================================*/
/* Domain: Id_Material                                          */
/*==============================================================*/
create domain Id_Material as numeric(9);

comment on domain Id_Material is
'УИД материала';

/*==============================================================*/
/* Domain: Id_MaterialType                                      */
/*==============================================================*/
create domain Id_MaterialType as numeric(4);

comment on domain Id_MaterialType is
'УИД типа материала';

/*==============================================================*/
/* Domain: Id_Part                                              */
/*==============================================================*/
create domain Id_Part as numeric(15);

comment on domain Id_Part is
'УИД детали';

/*==============================================================*/
/* Domain: Id_Person                                            */
/*==============================================================*/
create domain Id_Person as numeric(4);

comment on domain Id_Person is
'УИД Персоны';

/*==============================================================*/
/* Domain: Id_Project                                           */
/*==============================================================*/
create domain Id_Project as numeric(12);

comment on domain Id_Project is
'УИД Проекта';

/*==============================================================*/
/* Domain: Id_ProjectState                                      */
/*==============================================================*/
create domain Id_ProjectState as numeric(3);

comment on domain Id_ProjectState is
'УИД Состояния проекта';

/*==============================================================*/
/* Domain: Id_SettlementUnit                                    */
/*==============================================================*/
create domain Id_SettlementUnit as numeric(3);

comment on domain Id_SettlementUnit is
'УИД расчетной единицы';

/*==============================================================*/
/* Domain: Id_TrimmingType                                      */
/*==============================================================*/
create domain Id_TrimmingType as numeric(3);

comment on domain Id_TrimmingType is
'УИД типа торцовки';

/*==============================================================*/
/* Domain: Id_Wrapping                                          */
/*==============================================================*/
create domain Id_Wrapping as numeric(12);

comment on domain Id_Wrapping is
'УИД Упаковки';

/*==============================================================*/
/* Domain: Id_WrappingPart                                      */
/*==============================================================*/
create domain Id_WrappingPart as numeric(15);

comment on domain Id_WrappingPart is
'Деталь в упаковке';

/*==============================================================*/
/* Domain: LargeName                                            */
/*==============================================================*/
create domain LargeName as Varchar(300);

comment on domain LargeName is
'Наименование';

/*==============================================================*/
/* Domain: No                                                   */
/*==============================================================*/
create domain No as numeric(4);

comment on domain No is
'Номер';

/*==============================================================*/
/* Domain: ShortName                                            */
/*==============================================================*/
create domain ShortName as varchar(12);

comment on domain ShortName is
'Сокращенное наименование';

/*==============================================================*/
/* Domain: Weight                                               */
/*==============================================================*/
create domain Weight as numeric(4,3);

comment on domain Weight is
'Вес в киллограммах';

/*==============================================================*/
/* Table: ElementLinkage                                        */
/*==============================================================*/
create table ElementLinkage (
   Id_ElementLinkage    Id_ElementLinkage    not null,
   Id_Linkage           Id_Linkage           not null,
   Id_SubLinkage        Id_Linkage           null,
   Id_Part              Id_Part              null,
   Function             Comment              null,
   constraint PK_ELEMENTLINKAGE primary key (Id_ElementLinkage)
);

comment on table ElementLinkage is
'Элемент сборки';

comment on column ElementLinkage.Id_ElementLinkage is
'УИД элемента сборки';

comment on column ElementLinkage.Id_Linkage is
'УИД сборки-родителя';

comment on column ElementLinkage.Id_SubLinkage is
'УИД подчиненной cборки';

comment on column ElementLinkage.Id_Part is
'УИД Детали';

comment on column ElementLinkage.Function is
'Функция детали/сборки в сборке';

/*==============================================================*/
/* Index: ElementLinkage_unique                                 */
/*==============================================================*/
create unique index ElementLinkage_unique on ElementLinkage (
Id_ElementLinkage
);

/*==============================================================*/
/* Table: Linkage                                               */
/*==============================================================*/
create table Linkage (
   Id_Linkage           Id_Linkage           not null,
   Id_Project           Id_Project           null,
   Name                 LargeName            not null,
   Instruction          Comment              null,
   constraint PK_LINKAGE primary key (Id_Linkage)
);

comment on table Linkage is
'Сборка';

comment on column Linkage.Id_Linkage is
'УИД Сборки';

comment on column Linkage.Id_Project is
'УИД проекта';

/*==============================================================*/
/* Index: Linkage_unique                                        */
/*==============================================================*/
create unique index Linkage_unique on Linkage (
Id_Linkage
);

/*==============================================================*/
/* Table: Material                                              */
/*==============================================================*/
create table Material (
   Id_Material          Id_Material          not null,
   Id_MaterialType      Id_MaterialType      not null,
   Id_SettlementUnit    Id_SettlementUnit    not null,
   Name                 LargeName            not null,
   Code                 Code                 not null,
   Cost                 Cost                 not null,
   constraint PK_MATERIAL primary key (Id_Material)
);

comment on table Material is
'Материал изделия';

comment on column Material.Id_Material is
'УИД Материала';

comment on column Material.Id_MaterialType is
'УИД типа материала';

comment on column Material.Id_SettlementUnit is
'УИД расчетной единицы';

comment on column Material.Name is
'Наименование материала';

comment on column Material.Code is
'Код материала';

comment on column Material.Cost is
'Стоимость за единицу измерения';

/*==============================================================*/
/* Index: Material_unique                                       */
/*==============================================================*/
create unique index Material_unique on Material (
Id_Material
);

/*==============================================================*/
/* Table: MaterialType                                          */
/*==============================================================*/
create table MaterialType (
   Id_MaterialType      Id_MaterialType      not null,
   Name                 LargeName            not null,
   Code                 Code                 not null,
   constraint PK_MATERIALTYPE primary key (Id_MaterialType)
);

comment on table MaterialType is
'Вид материала (стекло, лдсп и тд.)';

comment on column MaterialType.Id_MaterialType is
'УИД типа материала';

comment on column MaterialType.Name is
'Наименование вида материала';

comment on column MaterialType.Code is
'Код вида материала';

/*==============================================================*/
/* Index: MaterialType_unique                                   */
/*==============================================================*/
create unique index MaterialType_unique on MaterialType (
Id_MaterialType
);

/*==============================================================*/
/* Table: Part                                                  */
/*==============================================================*/
create table Part (
   Id_Part              Id_Part              not null,
   Id_Material          Id_Material          not null,
   Id_TrimmingType      Id_TrimmingType      null,
   Id_TrimmingMaterial  Id_Material          null,
   Name                 LargeName            not null,
   Code                 Code                 not null,
   Weight               Weight               not null,
   Height               Dimension            not null,
   Width                Dimension            not null,
   Depth                Dimension            not null,
   IsImplement          Flag                 not null,
   constraint PK_PART primary key (Id_Part)
);

comment on table Part is
'Деталь';

comment on column Part.Id_Part is
'УИД Детали';

comment on column Part.Id_Material is
'УИД Материала';

comment on column Part.Id_TrimmingType is
'УИД типа торцовки';

comment on column Part.Id_TrimmingMaterial is
'УИД Материала торцовки';

comment on column Part.Name is
'Наименование детали';

comment on column Part.Code is
'Код детали';

comment on column Part.Weight is
'Вес в киллограммах';

comment on column Part.Height is
'Высота';

comment on column Part.Width is
'Ширина';

comment on column Part.Depth is
'Толщина/глубина';

comment on column Part.IsImplement is
'Фурнитура, стандартная деталь';

/*==============================================================*/
/* Index: Part_unique                                           */
/*==============================================================*/
create unique index Part_unique on Part (
Id_Part
);

/*==============================================================*/
/* Table: Person                                                */
/*==============================================================*/
create table Person (
   Id_Person            Id_Person            not null,
   FirstName            LargeName            not null,
   FamilyName           LargeName            not null,
   MiddleName           LargeName            null,
   constraint PK_PERSON primary key (Id_Person)
);

comment on table Person is
'Персоны (пользователи, исполнители и тд)';

comment on column Person.FirstName is
'Имя';

comment on column Person.FamilyName is
'Фамилия';

comment on column Person.MiddleName is
'Отчество';

/*==============================================================*/
/* Index: Person_unique                                         */
/*==============================================================*/
create unique index Person_unique on Person (
Id_Person
);

/*==============================================================*/
/* Table: Project                                               */
/*==============================================================*/
create table Project (
   Id_Project           Id_Project           not null,
   Id_Excecutor         Id_Person            not null,
   Id_ProjectState      Id_ProjectState      not null,
   DBegin               Date                 not null,
   DEnd                 Date                 null,
   Comment              Comment              null,
   constraint PK_PROJECT primary key (Id_Project)
);

comment on table Project is
'Проект';

comment on column Project.Id_Project is
'УИД проекта';

comment on column Project.Id_Excecutor is
'Ответственный исполнитель';

comment on column Project.Id_ProjectState is
'УИД Состояния проекта';

comment on column Project.DBegin is
'Дата начала';

comment on column Project.DEnd is
'Дата окончания';

comment on column Project.Comment is
'Комментарий';

/*==============================================================*/
/* Index: Project_unique                                        */
/*==============================================================*/
create unique index Project_unique on Project (
Id_Project
);

/*==============================================================*/
/* Table: ProjectState                                          */
/*==============================================================*/
create table ProjectState (
   Id_ProjectState      Id_ProjectState      not null,
   Name                 LargeName            not null,
   Code                 Code                 not null,
   constraint PK_PROJECTSTATE primary key (Id_ProjectState)
);

comment on table ProjectState is
'Статусы проекта (создан, в работе, в производстве, закончен и тд.)';

comment on column ProjectState.Id_ProjectState is
'УИД Состояния проекта';

comment on column ProjectState.Name is
'Наименование';

comment on column ProjectState.Code is
'Код';

/*==============================================================*/
/* Index: ProjectState_unique                                   */
/*==============================================================*/
create unique index ProjectState_unique on ProjectState (
Id_ProjectState
);

/*==============================================================*/
/* Table: SettlementUnit                                        */
/*==============================================================*/
create table SettlementUnit (
   Id_SettlementUnit    Id_SettlementUnit    not null,
   Name                 LargeName            not null,
   Code                 Code                 not null,
   ShortName            ShortName            not null,
   constraint PK_SETTLEMENTUNIT primary key (Id_SettlementUnit)
);

comment on table SettlementUnit is
'Расчетные единицы';

comment on column SettlementUnit.Id_SettlementUnit is
'УИД расчетной единицы';

comment on column SettlementUnit.Name is
'Наименование расчетной единицы';

comment on column SettlementUnit.Code is
'Код расчетной единицы';

comment on column SettlementUnit.ShortName is
'Сокращенное наименование';

/*==============================================================*/
/* Index: SettlementUnit_unique                                 */
/*==============================================================*/
create unique index SettlementUnit_unique on SettlementUnit (
Id_SettlementUnit
);

/*==============================================================*/
/* Table: TrimmingType                                          */
/*==============================================================*/
create table TrimmingType (
   Id_TrimmingType      Id_TrimmingType      not null,
   Name                 LargeName            null,
   Code                 Code                 not null,
   constraint PK_TRIMMINGTYPE primary key (Id_TrimmingType)
);

comment on table TrimmingType is
'Типы торцовки';

comment on column TrimmingType.Id_TrimmingType is
'УИД типа торцовки';

comment on column TrimmingType.Code is
'Код';

/*==============================================================*/
/* Index: TrimmingType_unique                                   */
/*==============================================================*/
create unique index TrimmingType_unique on TrimmingType (
Id_TrimmingType
);

/*==============================================================*/
/* Table: Wrapping                                              */
/*==============================================================*/
create table Wrapping (
   Id_Wrapping          Id_Wrapping          not null,
   Id_Project           Id_Project           not null,
   No                   No                   not null,
   constraint PK_WRAPPING primary key (Id_Wrapping)
);

comment on table Wrapping is
'Упаковка';

comment on column Wrapping.Id_Wrapping is
'УИД Упаковки';

comment on column Wrapping.Id_Project is
'УИД проекта';

comment on column Wrapping.No is
'Номер упаковки';

/*==============================================================*/
/* Index: Wrapping_unique                                       */
/*==============================================================*/
create unique index Wrapping_unique on Wrapping (
Id_Wrapping
);

/*==============================================================*/
/* Table: WrappingPart                                          */
/*==============================================================*/
create table WrappingPart (
   Id_WrappingPart      Id_WrappingPart      not null,
   Id_Part              Id_Part              null,
   Id_Wrapping          Id_Wrapping          null,
   constraint PK_WRAPPINGPART primary key (Id_WrappingPart)
);

comment on table WrappingPart is
'Детали в упаковке';

comment on column WrappingPart.Id_WrappingPart is
'Деталь в упаковке';

comment on column WrappingPart.Id_Part is
'УИД Детали';

comment on column WrappingPart.Id_Wrapping is
'УИД Упаковки';

/*==============================================================*/
/* Index: WrappingPart_unique                                   */
/*==============================================================*/
create unique index WrappingPart_unique on WrappingPart (
Id_WrappingPart
);

alter table ElementLinkage
   add constraint ElementLinkageSub_to_Linkage foreign key (Id_SubLinkage)
      references Linkage (Id_Linkage)
      on delete restrict on update restrict;

alter table ElementLinkage
   add constraint ElementLinkage_to_Linkage foreign key (Id_Linkage)
      references Linkage (Id_Linkage)
      on delete restrict on update restrict;

alter table ElementLinkage
   add constraint ElementLinkage_to_Part foreign key (Id_Part)
      references Part (Id_Part)
      on delete restrict on update restrict;

alter table Linkage
   add constraint Linkage_To_Project foreign key (Id_Project)
      references Project (Id_Project)
      on delete restrict on update restrict;

alter table Material
   add constraint Material_to_MaterialType foreign key (Id_MaterialType)
      references MaterialType (Id_MaterialType)
      on delete restrict on update restrict;

alter table Material
   add constraint Material_to_SettlementUnit foreign key (Id_SettlementUnit)
      references SettlementUnit (Id_SettlementUnit)
      on delete restrict on update restrict;

alter table Part
   add constraint Part_to_Material foreign key (Id_Material)
      references Material (Id_Material)
      on delete restrict on update restrict;

alter table Part
   add constraint Part_to_TrimmingType foreign key (Id_TrimmingType)
      references TrimmingType (Id_TrimmingType)
      on delete restrict on update restrict;

alter table Part
   add constraint FK_PART_REFERENCE_MATERIAL foreign key (Id_TrimmingMaterial)
      references Material (Id_Material)
      on delete restrict on update restrict;

alter table Project
   add constraint ProjectExecutor_to_Person foreign key (Id_Excecutor)
      references Person (Id_Person)
      on delete restrict on update restrict;

alter table Project
   add constraint Project_to_ProjectState foreign key (Id_ProjectState)
      references ProjectState (Id_ProjectState)
      on delete restrict on update restrict;

alter table Wrapping
   add constraint FK_WRAPPING_REFERENCE_PROJECT foreign key (Id_Project)
      references Project (Id_Project)
      on delete restrict on update restrict;

alter table WrappingPart
   add constraint FK_WRAPPING_REFERENCE_PART foreign key (Id_Part)
      references Part (Id_Part)
      on delete restrict on update restrict;

alter table WrappingPart
   add constraint FK_WRAPPING_REFERENCE_WRAPPING foreign key (Id_Wrapping)
      references Wrapping (Id_Wrapping)
      on delete restrict on update restrict;

