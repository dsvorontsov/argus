package jsf;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import jpa.entities.Projectsearch;
import jpa.session.ProjectsearchFacade;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;

@ManagedBean(name = "ProjectsearchController")
@ViewScoped
public class ProjectsearchController implements Serializable {

    private Projectsearch current;
    private DataModel items = null;
    @EJB
    private jpa.session.ProjectsearchFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Map<String, Object> parameters;
    private BigInteger idProject;

    public BigInteger getIdProject() {
        return idProject;
    }

    public void setIdProject(BigInteger idProject) {
        this.idProject = idProject;
    }

    public BigInteger getIdExecutor() {
        return idExecutor;
    }

    public void setIdExecutor(BigInteger idExecutor) {
        this.idExecutor = idExecutor;
    }
    private BigInteger idExecutor;

    public void setParameters() {
        Map<String, String> params;
        this.parameters.put("idProject", idProject);
        this.parameters.put("idExecutor", idExecutor);
        recreateModel();
    }

    public ProjectsearchController() {
        parameters = new HashMap<String, Object>();
        this.parameters.put("idProject", idProject);
        this.parameters.put("idExecutor", idExecutor);
    }

    public Projectsearch getSelected() {
        if (current == null) {
            current = new Projectsearch();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ProjectsearchFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }
    
    public void refresh() {
        recreateModel();
        java.math.BigInteger id = current.getIdProject();
        getFacade().detach(current);
        current = getFacade().find(id);
                
    }
    
    public String prepareView() {
        current = (Projectsearch) getItems().getRowData();
        return "ProjectView" + "?idProject=" + current.getIdProject() + "faces-redirect=true";
    }

    /* public String prepareCreate() {
     current = new ProjectsearchController();
     selectedItemIndex = -1;
     return "Create";
     }*/

    /* public String create() {
     try {
     getFacade().create(current);
     JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ProjectsearchCreated"));
     return prepareCreate();
     } catch (Exception e) {
     JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
     return null;
     }
     }*/
    public String prepareEdit() {
        current = (Projectsearch) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }


    /*  public String update() {
     try {
     getFacade().edit(current);
     JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ProjectsearchUpdated"));
     return "View";
     } catch (Exception e) {
     JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
     return null;
     }
     }*/

    /*   public String destroy() {
     current = (ProjectsearchController) getItems().getRowData();
     selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
     performDestroy();
     recreatePagination();
     recreateModel();
     return "List";
     }*/

    /* public String destroyAndView() {
     performDestroy();
     recreateModel();
     updateCurrentItem();
     if (selectedItemIndex >= 0) {
     return "View";
     } else {
     // all items were removed - go back to list
     recreateModel();
     return "List";
     }
     }*/

    /*    private void performDestroy() {
     try {
     getFacade().remove(current);
     JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ProjectsearchDeleted"));
     } catch (Exception e) {
     JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
     }
     }*/

    /*    private void updateCurrentItem() {
     int count = getFacade().count();
     if (selectedItemIndex >= count) {
     // selected index cannot be bigger than number of items:
     selectedItemIndex = count - 1;
     // go to previous page if last page disappeared:
     if (pagination.getPageFirstItem() >= count) {
     pagination.previousPage();
     }
     }
     if (selectedItemIndex >= 0) {
     current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
     }
     }*/
    public DataModel getItems() {
        if (items == null) {
            items = new ListDataModel(getFacade().findByNamedQuery("Projectsearch.findByCriteria", this.parameters));  //getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = ProjectsearchController.class)
    public static class ProjectsearchControllerConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProjectsearchController controller = (ProjectsearchController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "materialFromViewController");
            return controller.ejbFacade.find(getKey(value));
        }

        String getKey(String value) {
            String key;
            key = value;
            return key;
        }

        String getStringKey(java.math.BigInteger value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Projectsearch) {
                Projectsearch o = (Projectsearch) object;
                return getStringKey(o.getIdProject());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + ProjectsearchController.class.getName());
            }
        }
    }
}
