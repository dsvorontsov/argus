/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import jpa.entities.Linkage;
import jpa.entities.Part;
import jpa.entities.Project;
import jpa.entities.Wrapping;
import jpa.entities.Wrappingpart;
import jpa.session.ProjectFacade;
import net.sf.jasperreports.engine.JRException;

/**
 *
 * @author mitya
 */
@ManagedBean
@ViewScoped
public class ProjectViewController implements Serializable {
    
    @EJB
    private jpa.session.ProjectFacade ejbFacade;
    
    private BigInteger idProject;
    private Project current = null;
    
    public BigInteger getIdProject() {
        return idProject;
    }
    
    public void setIdProject(BigInteger idProject) {
        this.idProject = idProject;
    }
    
    private ProjectFacade getFacade() {
        return ejbFacade;
    }
    
    public Project getCurrent() {
        if (current == null && idProject != null) {
            current = (Project) getFacade().find(idProject);
        }
        return current;
    }
    
    public void removeLinkage(Linkage idLinkage) {

        //удаляем сборку
        current.getLinkageCollection().remove(idLinkage);

        //также нужно удалить детали из упаковок
       /* for (Part lp : idLinkage.getChildParts()) {
            for (Wrapping wp : current.getWrappingCollection()) {
                for (Wrappingpart pt : wp.getWrappingpartCollection()) {
                    if (pt.getIdPart().equals(lp)) {
                        
                    }
                }
            }
        }*/
        
        getFacade().edit(current);
    }
    
    public void update() {
        getFacade().edit(current);
        current = (Project) getFacade().find(idProject);
    }
    
    public void executeReport() throws JRException, IOException, NamingException, SQLException {
        jsf.ReportsManagedController re = new ReportsManagedController();
        re.exportToPdf(new HashMap(), (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse());
    }
    
    public ProjectViewController() {
    }
}
