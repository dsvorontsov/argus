/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

/**
 *
 * @author mitya
 */
@ManagedBean
@ViewScoped
public class ReportsManagedController {

    private JasperPrint jasperPrint;

    public ReportsManagedController() {
    }

    public void reportBuilder(HashMap hm) throws JRException, NamingException, SQLException {
        try {
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("jdbc/production-assistant");
            Connection connection;
            connection = ds.getConnection();
            connection.setAutoCommit(true);
            JasperReport jasperReport = JasperCompileManager.compileReport(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/reports/report2.jrxml"));
            jasperPrint = JasperFillManager.fillReport(jasperReport, hm, connection);
            connection.close();
        }
        catch (Exception e) {
        }

    }

    public void exportToPdf(HashMap hm,
            HttpServletResponse httpServletResponse) throws JRException, IOException, NamingException, SQLException {
        try {
            reportBuilder(hm);
              httpServletResponse.addHeader("Content-disposition", "attachment; filename=Employees_List.pdf;");
             ServletOutputStream servletStream = httpServletResponse.getOutputStream();
            
            JasperExportManager.exportReportToPdfFile(
                    jasperPrint, FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/reports").toString().concat("/simple_report.pdf"));
            JasperExportManager.exportReportToPdfStream(jasperPrint, servletStream);
             FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
        }
    }

    public void exportToXlsx(HashMap hm) throws JRException, IOException, NamingException, SQLException {
        reportBuilder(hm);
        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=Employees_List.xlsx");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JRXlsxExporter docxExporter = new JRXlsxExporter();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        docxExporter.exportReport();
        FacesContext.getCurrentInstance().responseComplete();
    }
}
