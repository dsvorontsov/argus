/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import jpa.entities.Elementlinkage;
import jpa.entities.Linkage;
import jpa.entities.Part;
import jpa.session.ElementlinkageFacade;
import jpa.session.LinkageFacade;
import jpa.session.PartFacade;
import jsf.util.JsfUtil;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mitya
 */
@ManagedBean(name = "partController")
@ViewScoped
public final class PartController implements Serializable {

    private DataModel items = null;
    @EJB
    private jpa.session.PartFacade ejbFacade;
    @EJB
    private jpa.session.LinkageFacade ejbLinkageFacade;
    private BigInteger elementCountProcess;

    public BigInteger getElementCountProcess() {
        return elementCountProcess;
    }

    public void setElementCountProcess(BigInteger elementCountProcess) {
        this.elementCountProcess = elementCountProcess;
    }

    public LinkageFacade getLinkageFacade() {
        return ejbLinkageFacade;
    }

    public ElementlinkageFacade getLinkageElementFacade() {
        return ejbLinkageElementFacade;
    }
    @EJB
    private jpa.session.ElementlinkageFacade ejbLinkageElementFacade;
    // @ManagedProperty(value = "#{ProjectsearchController.selected.idProject}")
    private Part current = null;
    //search parameters
    private String name = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    private String code = null;
    private Map<String, Object> parameters;

    public DataModel getItems() {
        if (items == null) {
            items = new ListDataModel(getFacade().findByNamedQuery("Part.findByCriteria", this.parameters));  //getPagination().createPageDataModel();
        }
        return items;
    }

    public void prepareSearch() {
        Map<String, String> params;
        this.parameters.put("name", name);
        this.parameters.put("code", code);
        recreateModel();
    }

    private PartFacade getFacade() {
        return ejbFacade;
    }

    public Part getCurrent() {
        if (current == null) {
            current = new Part();
        }
        return current;
    }

    public void prepareCreate() {
        if (current == null || current.getIdPart() != null) {
            current = new Part();
        }
        RequestContext.getCurrentInstance().execute("editDialogVar.show();");
    }

    public void prepareEdit() {
        current = (Part) getItems().getRowData();
        RequestContext.getCurrentInstance().execute("editDialogVar.show();");
    }

    public void create() {
        try {
            if (current.getIdPart() == null) {
                getFacade().create(current);
                recreateModel();
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("PartCreated"));
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public void prepareCopy() {
        current = (Part) getItems().getRowData();
        getFacade().detach(current);
        current.setIdPart(null);
        RequestContext.getCurrentInstance().execute("editDialogVar.show();");
    }

    public void addToLinkage(Linkage l) {
        Part selectedPart = (Part) getItems().getRowData();
        Elementlinkage elementLinkage;
        for (Iterator<Elementlinkage> i = l.getElementlinkageCollection().iterator(); i.hasNext();) {
            elementLinkage = i.next();
            if (elementLinkage.getIdPart() != null
                    && elementLinkage.getIdPart().getIdPart().compareTo(selectedPart.getIdPart()) == 0) {
                elementLinkage.setCountElement(elementLinkage.getCountElement().add(this.getElementCountProcess()));
                selectedPart = null;
                break;
            }
        }

        if (selectedPart != null) {
            elementLinkage = new Elementlinkage();
            elementLinkage.setIdPart(selectedPart);
            elementLinkage.setIdLinkage(l);
            elementLinkage.setCountElement(this.getElementCountProcess());
            getLinkageElementFacade().create(elementLinkage);
            l.getElementlinkageCollection().add(elementLinkage);
        }

        getLinkageFacade().edit(l);
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("PartDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public String prepareView() {
        current = (Part) getItems().getRowData();
        return "View";
    }

    private void recreateModel() {
        items = null;
    }

    public void destroy() {
        current = (Part) getItems().getRowData();
        performDestroy();
        recreateModel();
    }

    public void update() {
        getFacade().edit(current);
    }

    public PartController() {
        parameters = new HashMap<String, Object>();
        this.parameters.put("name", name);
        this.parameters.put("code", code);
        this.setElementCountProcess(BigInteger.ONE);
    }
}
