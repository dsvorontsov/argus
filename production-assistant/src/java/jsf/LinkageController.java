package jsf;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import jpa.entities.Elementlinkage;
import jpa.entities.Linkage;
import jpa.session.ElementlinkageFacade;
import jpa.session.LinkageFacade;
import jsf.util.JsfUtil;
import jsf.util.PaginationHelper;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "linkageController")
@ViewScoped
public class LinkageController implements Serializable {

    private Linkage current;
    private DataModel items = null;
    @EJB
    private jpa.session.LinkageFacade ejbFacade;
    @EJB
    private jpa.session.ElementlinkageFacade ejbElementFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Map<String, Object> parameters;
    private String name;
    private BigInteger idLinkage;
    private Elementlinkage elementLinkageToChange;
    private BigInteger elementCountProcess;

    public BigInteger getElementCountProcess() {
        return elementCountProcess;
    }

    public void setElementCountProcess(BigInteger elementCountProcess) {
        this.elementCountProcess = elementCountProcess;
    }

    public Elementlinkage getElementLinkageToChange() {
        return elementLinkageToChange;
    }

    public void setElementLinkageToChange(Elementlinkage elementLinkageToChange) {
        this.elementLinkageToChange = elementLinkageToChange;
    }

    public BigInteger getIdLinkage() {
        return idLinkage;
    }

    public void setIdLinkage(BigInteger idLinkage) {
        this.idLinkage = idLinkage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParameters() {
        this.parameters.put("name", this.name);
        recreateModel();
    }

    public LinkageController() {
        parameters = new HashMap<String, Object>();
        this.parameters.put("name", "-1");
        this.setElementCountProcess(BigInteger.ONE);
    }

    public Linkage getCurrent() {
        if (this.getIdLinkage() != null && current == null) {
            current = getFacade().find(this.getIdLinkage());
        }
        if (current == null) {
            current = new Linkage();
        }
        return current;
    }

    public Linkage getSelected() {
        if (current == null) {
            current = new Linkage();
            selectedItemIndex = -1;
        }
        return current;
    }

    private LinkageFacade getFacade() {
        return ejbFacade;
    }

    private ElementlinkageFacade getElementFacade() {
        return ejbElementFacade;
    }


    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public void prepareSearch() {
        this.parameters.clear();
        this.parameters.put("name", name);
        recreateModel();
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Linkage) getItems().getRowData();
        return "View" + "?idLinkage=" + current.getIdLinkage() + "faces-redirect=true";
    }

    public void prepareCreate() {
        if (current == null || current.getIdLinkage() != null) {
            current = new Linkage();
        }
        RequestContext.getCurrentInstance().execute("editDialogVar.show();");
    }

    public void create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("LinkageCreated"));
            recreateModel();
            RequestContext.getCurrentInstance().execute("editDialogVar.hide();");
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public void prepareEdit() {
        RequestContext.getCurrentInstance().execute("editDialogVar.show();");
    }

    public void update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LinkageUpdated"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    /**
     * Рекурсивное удаление сборки
     *
     * @param idLinkage
     */
    public void destroy(Linkage idLinkage) {
        try {
            for (Elementlinkage em : idLinkage.getElementlinkageCollection()) {
                if (em.getIdSublinkage() != null) {
                    Linkage ed = em.getIdSublinkage();
                    em.setIdSublinkage(null);
                    getElementFacade().edit(em);
                    destroy(ed);
                }
            }
            getFacade().remove(idLinkage);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LinkageDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public void destroy() {
        current = (Linkage) getItems().getRowData();
        destroy(current);
        recreateModel();
    }

    public DataModel getItems() {
        if (items == null) {
            items = new ListDataModel(getFacade().findByNamedQuery("Linkage.findByCriteria", this.parameters));
        }
        return items;
    }

    public void copyIntoProject(BigInteger iId_Project) {
        Map<String, Object> parameters;
        parameters = new HashMap<String, Object>();
        parameters.put("Id_Project", iId_Project);
        parameters.put("Id_Linkage", ((Linkage) getItems().getRowData()).getIdLinkage());
        getFacade().executeStoredProc("select CopyLinkageIntoProject(#Id_Project, #Id_Linkage)", parameters);
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public void addToLinkage() {

        Linkage selectedLinkage = (Linkage) getItems().getRowData();

        Elementlinkage elementLinkage;
        for (Iterator<Elementlinkage> i = this.getCurrent().getElementlinkageCollection().iterator(); i.hasNext();) {
            elementLinkage = i.next();
            if (elementLinkage.getIdSublinkage() != null
                    && elementLinkage.getIdSublinkage().getIdLinkage().compareTo(selectedLinkage.getIdLinkage()) == 0) {
                elementLinkage.setCountElement(elementLinkage.getCountElement().add(this.getElementCountProcess()));
                selectedLinkage = null;
                break;
            }
        }

        if (selectedLinkage != null) {
            elementLinkage = new Elementlinkage();
            elementLinkage.setIdSublinkage(selectedLinkage);
            elementLinkage.setIdLinkage(this.getCurrent());
            elementLinkage.setCountElement(this.getElementCountProcess());
            ejbElementFacade.create(elementLinkage);
            this.getCurrent().getElementlinkageCollection().add(elementLinkage);
        }

        ejbFacade.edit(this.getCurrent());
    }

    public void removeFromLinkage(Elementlinkage e) {
        current.getElementlinkageCollection().remove(e);
        getElementFacade().remove(e);
    }

    public void prepareChangeNotation(Elementlinkage e) {
        elementLinkageToChange = e;
        RequestContext.getCurrentInstance().execute("editElementLinkage.show();");
    }

    public void changeElementNotation() {
        getElementFacade().edit(elementLinkageToChange);
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);

    }

    @FacesConverter(forClass = LinkageController.class)
    public static class LinkageControllerConverter implements Converter {

        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LinkageController controller = (LinkageController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "LinkageController");
            return controller.ejbFacade.find(getKey(value));
        }

        String getKey(String value) {
            String key;
            key = value;
            return key;
        }

        String getStringKey(java.math.BigInteger value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Linkage) {
                Linkage o = (Linkage) object;
                return getStringKey(o.getIdLinkage());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + LinkageController.class.getName());
            }
        }
    }
}
