/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author mitya
 */
    public class ONConverter implements Converter {

        public ONConverter() {
            super();
        }

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            System.out.print("object convert!!!!!!!!!!!!");
            if (value.equals("true")) {
                return java.math.BigInteger.ONE;
            } else {
                return java.math.BigInteger.ZERO;
            }
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            System.out.print("string convert!!!!!!!!!!!!");
            if (value==java.math.BigInteger.ONE) {
                return "true";
            } else {
                return "false";
            }
        }
    }