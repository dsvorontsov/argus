/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import jpa.entities.Linkage;
import jpa.entities.Part;
import jpa.entities.Project;
import jpa.entities.Wrapping;
import jpa.entities.WrappingHaving;
import jpa.entities.Wrappingpart;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mitya
 */
@ManagedBean(name = "wrappingController")
@ViewScoped
public final class WrappingController implements Serializable {

    @ManagedProperty(value = "#{projectViewController.current}")
    private Project idProject;
    @ManagedProperty(value = "#{linkageController.current}")
    private Linkage idLinkage;
    @EJB
    private jpa.session.PartFacade ejbFacade;
    @EJB
    private jpa.session.LinkageFacade ejbLinkageFacade;
    @EJB
    private jpa.session.WrappingFacade ejbWrappingFacade;
    @EJB
    private jpa.session.WrappingPartFacade ejbWrappingPartFacade;
    @EJB
    private jpa.session.PartFacade ejbPartFacade;
    private WrappingHaving current;
    private BigInteger elementCountProcess;
    private Wrapping wrapping;
    private DataModel wrappingItems = null;
    private DataModel wrappingFreeItems = null;
    private BigInteger partAddToWrapping = null;
    private BigInteger partAddToWrappingCount;
    private BigInteger partAddToWrappingCountMax = BigInteger.ZERO;

    public BigInteger getPartAddToWrappingCountMax() {
        return partAddToWrappingCountMax;
    }

    public BigInteger getPartAddToWrappingCount() {
        return partAddToWrappingCount;
    }

    public void setPartAddToWrappingCount(BigInteger partAddToWrappingCount) {
        this.partAddToWrappingCount = partAddToWrappingCount;
    }

    public BigInteger getPartAddToWrapping() {

        return partAddToWrapping;
    }

    public WrappingHaving getCurrent() {
        return this.getIdProject() == null ? this.getIdLinkage() : this.getIdProject();
    }

    public void setPartAddToWrapping(BigInteger partAddToWrapping) {
        ArrayList<Map.Entry> ws = (ArrayList<Map.Entry>) wrappingFreeItems.getWrappedData();
        Map.Entry<Part, BigInteger> e;
        for (Iterator<Map.Entry> i = ws.iterator(); i.hasNext();) {
            e = i.next();
            if (e.getKey().getIdPart().toBigInteger().equals(partAddToWrapping)) {
                partAddToWrappingCountMax = e.getValue();
            }
        }

        this.partAddToWrapping = partAddToWrapping;
    }

    public DataModel getWrappingFreeItems() {

        HashMap<Part, BigInteger> wp = new HashMap(this.getCurrent().getChildParts());

        for (Entry<Part, BigInteger> wd : this.getCurrent().getChildWrappingParts().entrySet()) {
            if (wp.containsKey(wd.getKey())) {
                wp.put(wd.getKey(), wp.get(wd.getKey()).subtract(wd.getValue()));
            }
        }

        wrappingFreeItems = new ListDataModel(new ArrayList<Map.Entry>(wp.entrySet()));

        return wrappingFreeItems;
    }

    public DataModel getWrappingItems() {
        if (wrappingItems == null) {
            wrappingItems = new ListDataModel(new ArrayList<Wrapping>(this.getCurrent().getWrappingCollection()));
        }
        return wrappingItems;
    }

    public Wrapping getWrapping() {
        if (wrapping == null) {
            wrapping = new Wrapping();
        }
        return wrapping;
    }

    public void setWrapping(Wrapping wrapping) {
        this.wrapping = wrapping;
    }

    public Linkage getIdLinkage() {
        return idLinkage;
    }

    public void setIdLinkage(Linkage idLinkage) {
        this.idLinkage = idLinkage;
    }

    public Project getIdProject() {
        return idProject;
    }

    public void setIdProject(Project idProject) {
        this.idProject = idProject;
    }

    public void prepareCreateWrapping() {
        this.wrapping = new Wrapping();
        if (this.getCurrent() instanceof Project) {
            this.wrapping.setIdProject((Project) this.getCurrent());
        } else {
            this.wrapping.setIdLinkage((Linkage) this.getCurrent());
        }
        //Part tmpPart;
        //BigInteger[] a;
        //ArrayList<Part, BigInteger[]> al=new ArrayList <Part, BigInteger[]>();

        /*    Map<Part, BigInteger[]> hashmap = new HashMap<Part, BigInteger[]>();
         ArrayList<Entry<Part, BigInteger[]>> cl = new ArrayList<Entry<Part, BigInteger[]>>();
         //соберем все детали, входящие в проект
         for (Iterator<Linkage> i = this.getCurrent().getLinkageCollection().iterator(); i.hasNext();) {
         for (Iterator<Part> j = i.next().getChildParts().iterator(); j.hasNext();) {
         tmpPart = j.next();
         if (hashmap.containsKey(tmpPart)) {
         a = new BigInteger[]{hashmap.get(tmpPart)[0].add(BigInteger.ONE), BigInteger.ZERO};
         } else {
         a = new BigInteger[]{BigInteger.ONE, BigInteger.ZERO};
         }

         hashmap.put(tmpPart, a);
         }
         }*/
//        wrappingItems = new ListDataModel(new ArrayList );
        RequestContext.getCurrentInstance().execute("dlgWrapping.show();");
    }

    public void createWrapping() {
        if (wrapping.getIdProject() != null) {
            Wrappingpart wrappingPart;

            if (wrapping.getNo() == null) {
                wrapping.setNo(java.math.BigInteger.ZERO);
            }

            ejbWrappingFacade.create(wrapping);

            //List<Part> partsTarget = wrappingParts.getTarget();
            //for (Iterator<Part> i = partsTarget.iterator(); i.hasNext();) {
            // wrappingPart = new Wrappingpart();
            //wrappingPart.setIdPart((Part) i.next());
            //wrappingPart.setIdWrapping(this.wrapping);
            //ejbWrappingPartFacade.create(wrappingPart);
            //this.wrapping.getWrappingpartCollection().add(wrappingPart);
            // }
            getCurrent().getWrappingCollection().add(wrapping);
            wrappingItems = null;
            RequestContext.getCurrentInstance().execute("dlgWrapping.hide();");
            // wrappingItems = null;
        }
    }

    public void deleteWrapping() {
        Wrapping wrapping = (Wrapping) getWrappingItems().getRowData();
        for (Iterator<Wrappingpart> i = wrapping.getWrappingpartCollection().iterator(); i.hasNext();) {
            ejbWrappingPartFacade.remove(i.next());
        }

        wrapping.getWrappingpartCollection().clear();

        getCurrent().getWrappingCollection().remove(wrapping);

        ejbWrappingFacade.remove(wrapping);

        wrappingItems = null;
    }

    public void deletePartFromWrapping(Wrappingpart idWrappingPart) {
        wrapping = ((Wrapping) getWrappingItems().getRowData());
        wrapping.getWrappingpartCollection().remove(idWrappingPart);
        ejbWrappingPartFacade.remove(idWrappingPart);
        this.wrappingFreeItems = null;
    }

    public void prepareAddPartToWrapping() {
        wrapping = ((Wrapping) getWrappingItems().getRowData());
        RequestContext.getCurrentInstance().execute("dlgAddPart.show();");
    }

    public void addPartToWrapping() {
        if (this.partAddToWrappingCount.compareTo(BigInteger.ONE) >= 0) {
            Wrappingpart wrappingPart = null;
            for (Iterator<Wrappingpart> i = wrapping.getWrappingpartCollection().iterator(); i.hasNext();) {
                wrappingPart = i.next();
                if (wrappingPart.getIdPart().getIdPart().toBigInteger().equals(this.partAddToWrapping)) {
                    wrappingPart.setCountElement((wrappingPart.getCountElement().add(this.partAddToWrappingCount)));
                    break;
                } else {
                    wrappingPart = null;
                }
            }

            if (wrappingPart == null) {
                wrappingPart = new Wrappingpart();
                Part part = ejbPartFacade.find(new BigDecimal(this.partAddToWrapping));

                wrappingPart.setIdPart(part);

                wrappingPart.setIdWrapping(this.wrapping);
                wrappingPart.setCountElement(this.partAddToWrappingCount);
                ejbWrappingPartFacade.create(wrappingPart);
                this.wrapping.getWrappingpartCollection().add(wrappingPart);

            }

            this.wrappingFreeItems = null;
        }
        RequestContext.getCurrentInstance().execute("dlgAddPart.hide();");
    }

    public WrappingController() {
    }
}
