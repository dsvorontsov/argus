/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Projectstate;

/**
 *
 * @author mitya
 */
@Stateless
public class ProjectstateFacade extends AbstractFacade<Projectstate> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProjectstateFacade() {
        super(Projectstate.class);
    }
    
}
