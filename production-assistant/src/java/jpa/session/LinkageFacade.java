/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Linkage;

/**
 *
 * @author mitya
 */
@Stateless
public class LinkageFacade extends AbstractFacade<Linkage> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LinkageFacade() {
        super(Linkage.class);
    }
    
}
