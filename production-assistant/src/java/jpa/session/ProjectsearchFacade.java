/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Projectsearch;

/**
 *
 * @author mitya
 */
@Stateless
public class ProjectsearchFacade extends AbstractFacade<Projectsearch> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProjectsearchFacade() {
        super(Projectsearch.class);
    }
    
}
