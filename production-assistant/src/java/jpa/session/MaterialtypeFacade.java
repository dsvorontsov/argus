/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Materialtype;

/**
 *
 * @author mitya
 */
@Stateless
public class MaterialtypeFacade extends AbstractFacade<Materialtype> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MaterialtypeFacade() {
        super(Materialtype.class);
    }
    
}
