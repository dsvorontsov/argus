/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Trimmingtype;

/**
 *
 * @author mitya
 */
@Stateless
public class TrimmingtypeFacade extends AbstractFacade<Trimmingtype> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TrimmingtypeFacade() {
        super(Trimmingtype.class);
    }
    
}
