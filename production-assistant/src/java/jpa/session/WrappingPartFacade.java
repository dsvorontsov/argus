/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Wrappingpart;

/**
 *
 * @author mitya
 */
@Stateless
public class WrappingPartFacade extends AbstractFacade<Wrappingpart> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WrappingPartFacade() {
        super(Wrappingpart.class);
    }
    
}
