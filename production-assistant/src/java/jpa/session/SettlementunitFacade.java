/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Settlementunit;

/**
 *
 * @author mitya
 */
@Stateless
public class SettlementunitFacade extends AbstractFacade<Settlementunit> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SettlementunitFacade() {
        super(Settlementunit.class);
    }
    
}
