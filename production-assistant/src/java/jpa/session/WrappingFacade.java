/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.Wrapping;

/**
 *
 * @author mitya
 */
@Stateless
public class WrappingFacade extends AbstractFacade<Wrapping> {
    @PersistenceContext(unitName = "production-assistantPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WrappingFacade() {
        super(Wrapping.class);
    }
    
}
