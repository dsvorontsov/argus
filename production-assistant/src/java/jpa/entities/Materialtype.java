/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator (name="idMaterialtype_seq", sequenceName="Id_MaterialType_seq", allocationSize=1)
@Table(name = "materialtype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materialtype.findAll", query = "SELECT m FROM Materialtype m"),
    @NamedQuery(name = "Materialtype.findByIdMaterialtype", query = "SELECT m FROM Materialtype m WHERE m.idMaterialtype = :idMaterialtype"),
    @NamedQuery(name = "Materialtype.findByName", query = "SELECT m FROM Materialtype m WHERE m.name = :name"),
    @NamedQuery(name = "Materialtype.findByCode", query = "SELECT m FROM Materialtype m WHERE m.code = :code")})
 
public class Materialtype implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idMaterialtype_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_materialtype")
    private BigDecimal idMaterialtype;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "code")
    private String code;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMaterialtype", fetch = FetchType.LAZY)
    private Collection<Material> materialCollection;

    public Materialtype() {
    }

    public Materialtype(BigDecimal idMaterialtype) {
        this.idMaterialtype = idMaterialtype;
    }

    public Materialtype(BigDecimal idMaterialtype, String name, String code) {
        this.idMaterialtype = idMaterialtype;
        this.name = name;
        this.code = code;
    }

    public BigDecimal getIdMaterialtype() {
        return idMaterialtype;
    }

    public void setIdMaterialtype(BigDecimal idMaterialtype) {
        this.idMaterialtype = idMaterialtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public Collection<Material> getMaterialCollection() {
        return materialCollection;
    }

    public void setMaterialCollection(Collection<Material> materialCollection) {
        this.materialCollection = materialCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMaterialtype != null ? idMaterialtype.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materialtype)) {
            return false;
        }
        Materialtype other = (Materialtype) object;
        if ((this.idMaterialtype == null && other.idMaterialtype != null) || (this.idMaterialtype != null && !this.idMaterialtype.equals(other.idMaterialtype))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Materialtype[ idMaterialtype=" + idMaterialtype + " ]";
    }
    
}
