/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@Table(name = "project")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p"),
    @NamedQuery(name = "Project.findByIdProject", query = "SELECT p FROM Project p WHERE p.idProject = :idProject"),
    @NamedQuery(name = "Project.findByDbegin", query = "SELECT p FROM Project p WHERE p.dbegin = :dbegin"),
    @NamedQuery(name = "Project.findByDend", query = "SELECT p FROM Project p WHERE p.dend = :dend"),
    @NamedQuery(name = "Project.findByComment", query = "SELECT p FROM Project p WHERE p.comment = :comment")})
public class Project implements Serializable, WrappingHaving {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_project")
    private BigInteger idProject;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dbegin")
    @Temporal(TemporalType.DATE)
    private Date dbegin;
    @Column(name = "dend")
    @Temporal(TemporalType.DATE)
    private Date dend;
    @Size(max = 2147483647)
    @Column(name = "comment")
    private String comment;
    @JoinColumn(name = "id_projectstate", referencedColumnName = "id_projectstate")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Projectstate idProjectstate;
    @JoinColumn(name = "id_executor", referencedColumnName = "id_person")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Person idExecutor;
    @OneToMany(mappedBy = "idProject", fetch = FetchType.LAZY)
    private Collection<Linkage> linkageCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProject", fetch = FetchType.LAZY)
    private Collection<Wrapping> wrappingCollection;

    public Project() {
    }

    public Project(BigInteger idProject) {
        this.idProject = idProject;
    }

    public Project(BigInteger idProject, Date dbegin) {
        this.idProject = idProject;
        this.dbegin = dbegin;
    }

    public BigInteger getIdProject() {
        return idProject;
    }

    public void setIdProject(BigInteger idProject) {
        this.idProject = idProject;
    }

    public Date getDbegin() {
        return dbegin;
    }

    public void setDbegin(Date dbegin) {
        this.dbegin = dbegin;
    }

    public Date getDend() {
        return dend;
    }

    public void setDend(Date dend) {
        this.dend = dend;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Projectstate getIdProjectstate() {
        return idProjectstate;
    }

    public void setIdProjectstate(Projectstate idProjectstate) {
        this.idProjectstate = idProjectstate;
        if (this.idProjectstate.getCode().equals("ENDED")) {
            this.setDend(new Date());
        } else {
            this.setDend(null);
        }
    }

    public Person getIdExecutor() {
        return idExecutor;
    }

    public void setIdExecutor(Person idExecutor) {
        this.idExecutor = idExecutor;
    }

    @XmlTransient
    public Collection<Linkage> getLinkageCollection() {
        return linkageCollection;
    }

    public void setLinkageCollection(Collection<Linkage> linkageCollection) {
        this.linkageCollection = linkageCollection;

    }

    @Override
    public HashMap<Part, BigInteger> getChildParts() {

        HashMap<Part, BigInteger> listParts = new HashMap<Part, BigInteger>();

        for (Linkage ls : this.getLinkageCollection()) {

            for (Map.Entry<Part, BigInteger> se : ls.getChildParts().entrySet()) {
                listParts.put(se.getKey(), se.getValue().add(listParts.get(se.getKey()) == null ? BigInteger.ZERO : listParts.get(se.getKey())));
            }

        }
        return listParts;
    }

    @Override
    public HashMap<Part, BigInteger> getChildWrappingParts() {
        HashMap<Part, BigInteger> listParts = new HashMap<Part, BigInteger>();

        for (Wrapping wp : this.getWrappingCollection()) {
            for (Wrappingpart wt : wp.getWrappingpartCollection()) {
                listParts.put(wt.getIdPart(), wt.getCountElement().add(listParts.get(wt.getIdPart()) == null ? BigInteger.ZERO : listParts.get(wt.getIdPart())));
            }
        }
        return listParts;
    }

    @XmlTransient
    @Override
    public Collection<Wrapping> getWrappingCollection() {
        return wrappingCollection;
    }

    public void setWrappingCollection(Collection<Wrapping> wrappingCollection) {
        this.wrappingCollection = wrappingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProject != null ? idProject.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.idProject == null && other.idProject != null) || (this.idProject != null && !this.idProject.equals(other.idProject))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Project[ idProject=" + idProject + " ]";
    }

    public Object getWrappingItems() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
