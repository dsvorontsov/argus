/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator(name = "idLinkage_seq", sequenceName = "Id_Linkage_seq", allocationSize = 1)
@Table(name = "linkage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Linkage.findAll", query = "SELECT l FROM Linkage l"),
    @NamedQuery(name = "Linkage.findByIdLinkage", query = "SELECT l FROM Linkage l WHERE l.idLinkage = :idLinkage"),
    @NamedQuery(name = "Linkage.findByName", query = "SELECT l FROM Linkage l WHERE l.name = :name"),
    @NamedQuery(name = "Linkage.findByInstruction", query = "SELECT l FROM Linkage l WHERE l.instruction = :instruction"),
    @NamedQuery(name = "Linkage.findByCriteria", query = "SELECT m "
            + "  FROM Linkage m "
            + " WHERE (upper(m.name)      like  upper(concat(concat('%', :name), '%'))"
            + "    or  :name       is null)")})
public class Linkage implements Serializable, WrappingHaving {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idLinkage_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_linkage")
    private BigInteger idLinkage;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @Size(max = 2147483647)
    @Column(name = "instruction")
    private String instruction;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLinkage", fetch = FetchType.LAZY)
    private Collection<Elementlinkage> elementlinkageCollection;
    @OneToMany(mappedBy = "idSublinkage", fetch = FetchType.LAZY)
    private Collection<Elementlinkage> elementlinkageCollection1;
    @JoinColumn(name = "id_project", referencedColumnName = "id_project")
    @ManyToOne(fetch = FetchType.LAZY)
    private Project idProject;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLinkage", fetch = FetchType.LAZY)
    private Collection<Wrapping> wrappingCollection;

    public Linkage() {
    }

    public Linkage(BigInteger idLinkage) {
        this.idLinkage = idLinkage;
    }

    public Linkage(BigInteger idLinkage, String name) {
        this.idLinkage = idLinkage;
        this.name = name;
    }

    @Override
    public HashMap<Part, BigInteger> getChildParts() {

        HashMap<Part, BigInteger> listParts = new HashMap<Part, BigInteger>();

        for (Elementlinkage e : this.elementlinkageCollection) {
            if (e.getIdPart() != null) {
                listParts.put(e.getIdPart(), e.getCountElement().add(listParts.get(e.getIdPart()) == null ? BigInteger.ZERO : listParts.get(e.getIdPart())));
            }
            if (e.getIdSublinkage() != null) {
                for (Entry<Part, BigInteger> se : e.getIdSublinkage().getChildParts().entrySet()) {
                    listParts.put(se.getKey(), se.getValue().add(listParts.get(se.getKey()) == null ? BigInteger.ZERO : listParts.get(se.getKey())));
                }
            }
        }
        return listParts;
    }

    @XmlTransient
    @Override
    public Collection<Wrapping> getWrappingCollection() {
        return wrappingCollection;
    }

    @Override
    public HashMap<Part, BigInteger> getChildWrappingParts() {
        HashMap<Part, BigInteger> listParts = new HashMap<Part, BigInteger>();

        for (Wrapping wp : this.getWrappingCollection()) {
            for (Wrappingpart wt : wp.getWrappingpartCollection()) {
                listParts.put(wt.getIdPart(), wt.getCountElement().add(listParts.get(wt.getIdPart()) == null ? BigInteger.ZERO : listParts.get(wt.getIdPart())));
            }
        }

        return listParts;
    }

    public BigInteger getIdLinkage() {
        return idLinkage;
    }

    public void setIdLinkage(BigInteger idLinkage) {
        this.idLinkage = idLinkage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    @XmlTransient
    public Collection<Elementlinkage> getElementlinkageCollection() {
        return elementlinkageCollection;
    }

    public void setElementlinkageCollection(Collection<Elementlinkage> elementlinkageCollection) {
        this.elementlinkageCollection = elementlinkageCollection;
    }

    @XmlTransient
    public Collection<Elementlinkage> getElementlinkageCollection1() {
        return elementlinkageCollection1;
    }

    public void setElementlinkageCollection1(Collection<Elementlinkage> elementlinkageCollection1) {
        this.elementlinkageCollection1 = elementlinkageCollection1;
    }

    public Project getIdProject() {
        return idProject;
    }

    public void setIdProject(Project idProject) {
        this.idProject = idProject;
    }

    public Project getIdProjectParent() {
        Project idProjectParent = new Project();
        for (Elementlinkage em : this.getElementlinkageCollection1()) {
            if (em.getIdLinkage().idProject != null) {
                idProjectParent = em.getIdLinkage().idProject;
            } else {
                idProjectParent = em.getIdLinkage().getIdProjectParent();
            }
        }
        return idProjectParent;
    }

    public Linkage getIdLinkageParent() throws Exception {
        Linkage idLinkageParent = new Linkage();

        if (this.getElementlinkageCollection1().size() > 1) {
            throw new Exception("Ошибка конструкции сборок: сборка является подсборкой более чем у одной сборки");
        }

        for (Elementlinkage em : this.getElementlinkageCollection1()) {
            idLinkageParent = em.getIdLinkage();
        }
        return idLinkageParent;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLinkage != null ? idLinkage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Linkage)) {
            return false;
        }
        Linkage other = (Linkage) object;
        if ((this.idLinkage == null && other.idLinkage != null) || (this.idLinkage != null && !this.idLinkage.equals(other.idLinkage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Linkage[ idLinkage=" + idLinkage + " ]";
    }
}
