/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator (name="idWrapping_seq", sequenceName="Id_Wrapping_seq", allocationSize=1)
@Table(name = "wrapping")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wrapping.findAll", query = "SELECT w FROM Wrapping w"),
    @NamedQuery(name = "Wrapping.findByIdWrapping", query = "SELECT w FROM Wrapping w WHERE w.idWrapping = :idWrapping"),
    @NamedQuery(name = "Wrapping.findByNo", query = "SELECT w FROM Wrapping w WHERE w.no = :no")})
public class Wrapping implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idWrapping_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_wrapping")
    private BigDecimal idWrapping;
    @Basic(optional = false)
    @NotNull
    @Column(name = "no")
    private BigInteger no;
    @OneToMany(mappedBy = "idWrapping", fetch = FetchType.LAZY)
    private Collection<Wrappingpart> wrappingpartCollection;
    @JoinColumn(name = "id_project", referencedColumnName = "id_project")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Project idProject;
    @JoinColumn(name = "id_linkage", referencedColumnName = "id_linkage")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Linkage idLinkage;

    public Linkage getIdLinkage() {
        return idLinkage;
    }

    public void setIdLinkage(Linkage idLinkage) {
        this.idLinkage = idLinkage;
    }

    public Wrapping() {
    }

    public Wrapping(BigDecimal idWrapping) {
        this.idWrapping = idWrapping;
    }

    public Wrapping(BigDecimal idWrapping, BigInteger no) {
        this.idWrapping = idWrapping;
        this.no = no;
    }

    public BigDecimal getIdWrapping() {
        return idWrapping;
    }

    public void setIdWrapping(BigDecimal idWrapping) {
        this.idWrapping = idWrapping;
    }

    public BigInteger getNo() {
        return no;
    }

    public void setNo(BigInteger no) {
        this.no = no;
    }

    @XmlTransient
    public Collection<Wrappingpart> getWrappingpartCollection() {
        return wrappingpartCollection;
    }

    public void setWrappingpartCollection(Collection<Wrappingpart> wrappingpartCollection) {
        this.wrappingpartCollection = wrappingpartCollection;
    }

    public Project getIdProject() {
        return idProject;
    }

    public void setIdProject(Project idProject) {
        this.idProject = idProject;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idWrapping != null ? idWrapping.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wrapping)) {
            return false;
        }
        Wrapping other = (Wrapping) object;
        if ((this.idWrapping == null && other.idWrapping != null) || (this.idWrapping != null && !this.idWrapping.equals(other.idWrapping))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Wrapping[ idWrapping=" + idWrapping + " ]";
    }
    
}
