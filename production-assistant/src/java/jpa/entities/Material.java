/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator (name="idMaterial_seq", sequenceName="Id_Material_seq", allocationSize=1)
@Table(name = "material")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Material.findAll", query = "SELECT m FROM Material m"),
    @NamedQuery(name = "Material.findByIdMaterial", query = "SELECT m FROM Material m WHERE m.idMaterial = :idMaterial"),
    @NamedQuery(name = "Material.findByName", query = "SELECT m FROM Material m WHERE m.name = :name"),
    @NamedQuery(name = "Material.findByCode", query = "SELECT m FROM Material m WHERE m.code = :code"),
    @NamedQuery(name = "Material.findByCost", query = "SELECT m FROM Material m WHERE m.cost = :cost")})
public class Material implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idMaterial_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_material")
    private BigDecimal idMaterial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cost")
    private BigDecimal cost;
    @OneToMany(mappedBy = "idTrimmingmaterial", fetch = FetchType.LAZY)
    private Collection<Part> partCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMaterial", fetch = FetchType.LAZY)
    private Collection<Part> partCollection1;
    @JoinColumn(name = "id_settlementunit", referencedColumnName = "id_settlementunit")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Settlementunit idSettlementunit;
    @JoinColumn(name = "id_materialtype", referencedColumnName = "id_materialtype")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Materialtype idMaterialtype;

    public Material() {
    }

    public Material(BigDecimal idMaterial) {
        this.idMaterial = idMaterial;
    }

    public Material(BigDecimal idMaterial, String name, String code, BigDecimal cost) {
        this.idMaterial = idMaterial;
        this.name = name;
        this.code = code;
        this.cost = cost;
    }

    public BigDecimal getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(BigDecimal idMaterial) {
        this.idMaterial = idMaterial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @XmlTransient
    public Collection<Part> getPartCollection() {
        return partCollection;
    }

    public void setPartCollection(Collection<Part> partCollection) {
        this.partCollection = partCollection;
    }

    @XmlTransient
    public Collection<Part> getPartCollection1() {
        return partCollection1;
    }

    public void setPartCollection1(Collection<Part> partCollection1) {
        this.partCollection1 = partCollection1;
    }

    public Settlementunit getIdSettlementunit() {
        return idSettlementunit;
    }

    public void setIdSettlementunit(Settlementunit idSettlementunit) {
        this.idSettlementunit = idSettlementunit;
    }

    public Materialtype getIdMaterialtype() {
        return idMaterialtype;
    }

    public void setIdMaterialtype(Materialtype idMaterialtype) {
        this.idMaterialtype = idMaterialtype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMaterial != null ? idMaterial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Material)) {
            return false;
        }
        Material other = (Material) object;
        if ((this.idMaterial == null && other.idMaterial != null) || (this.idMaterial != null && !this.idMaterial.equals(other.idMaterial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Material[ idMaterial=" + idMaterial + " ]";
    }
    
}
