/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator (name="idSettlementunit_seq", sequenceName="Id_SettlementUnit_seq", allocationSize=1)
@Table(name = "settlementunit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Settlementunit.findAll", query = "SELECT s FROM Settlementunit s"),
    @NamedQuery(name = "Settlementunit.findByIdSettlementunit", query = "SELECT s FROM Settlementunit s WHERE s.idSettlementunit = :idSettlementunit"),
    @NamedQuery(name = "Settlementunit.findByName", query = "SELECT s FROM Settlementunit s WHERE s.name = :name"),
    @NamedQuery(name = "Settlementunit.findByCode", query = "SELECT s FROM Settlementunit s WHERE s.code = :code"),
    @NamedQuery(name = "Settlementunit.findByShortname", query = "SELECT s FROM Settlementunit s WHERE s.shortname = :shortname")})
public class Settlementunit implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idSettlementunit_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_settlementunit")
    private BigDecimal idSettlementunit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "shortname")
    private String shortname;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSettlementunit", fetch = FetchType.LAZY)
    private Collection<Material> materialCollection;

    public Settlementunit() {
    }

    public Settlementunit(BigDecimal idSettlementunit) {
        this.idSettlementunit = idSettlementunit;
    }

    public Settlementunit(BigDecimal idSettlementunit, String name, String code, String shortname) {
        this.idSettlementunit = idSettlementunit;
        this.name = name;
        this.code = code;
        this.shortname = shortname;
    }

    public BigDecimal getIdSettlementunit() {
        return idSettlementunit;
    }

    public void setIdSettlementunit(BigDecimal idSettlementunit) {
        this.idSettlementunit = idSettlementunit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    @XmlTransient
    public Collection<Material> getMaterialCollection() {
        return materialCollection;
    }

    public void setMaterialCollection(Collection<Material> materialCollection) {
        this.materialCollection = materialCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSettlementunit != null ? idSettlementunit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Settlementunit)) {
            return false;
        }
        Settlementunit other = (Settlementunit) object;
        if ((this.idSettlementunit == null && other.idSettlementunit != null) || (this.idSettlementunit != null && !this.idSettlementunit.equals(other.idSettlementunit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Settlementunit[ idSettlementunit=" + idSettlementunit + " ]";
    }
    
}
