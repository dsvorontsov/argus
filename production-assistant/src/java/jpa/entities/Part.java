/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator(name = "idPart_seq", sequenceName = "Id_Part_seq", allocationSize = 1)
@Table(name = "part")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Part.findAll", query = "SELECT p FROM Part p"),
    @NamedQuery(name = "Part.findByIdPart", query = "SELECT p FROM Part p WHERE p.idPart = :idPart"),
    @NamedQuery(name = "Part.findByName", query = "SELECT p FROM Part p WHERE p.name = :name"),
    @NamedQuery(name = "Part.findByCode", query = "SELECT p FROM Part p WHERE p.code = :code"),
    @NamedQuery(name = "Part.findByWeight", query = "SELECT p FROM Part p WHERE p.weight = :weight"),
    @NamedQuery(name = "Part.findByHeight", query = "SELECT p FROM Part p WHERE p.height = :height"),
    @NamedQuery(name = "Part.findByWidth", query = "SELECT p FROM Part p WHERE p.width = :width"),
    @NamedQuery(name = "Part.findByDepth", query = "SELECT p FROM Part p WHERE p.depth = :depth"),
    @NamedQuery(name = "Part.findByIsimplement", query = "SELECT p FROM Part p WHERE p.isimplement = :isimplement"),
    @NamedQuery(name = "Part.findByCriteria", query = "SELECT p FROM Part p "
                                                    + "WHERE (upper (p.code) like  upper (concat ('%', concat (:code, '%'))) or :code is null)"
                                                    + "  and (upper (p.name) like  upper (concat ('%', concat (:name, '%'))) or :name is null)")})

public class Part implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idPart_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_part")
    private BigDecimal idPart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "weight")
    private BigDecimal weight;
    @Basic(optional = false)
    @NotNull
    @Column(name = "height")
    private BigInteger height;
    @Basic(optional = false)
    @NotNull
    @Column(name = "width")
    private BigInteger width;
    @Basic(optional = false)
    @NotNull
    @Column(name = "depth")
    private BigInteger depth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isimplement")
    private BigInteger isimplement;
    @OneToMany(mappedBy = "idPart", fetch = FetchType.LAZY)
    private Collection<Wrappingpart> wrappingpartCollection;
    @OneToMany(mappedBy = "idPart", fetch = FetchType.LAZY)
    private Collection<Elementlinkage> elementlinkageCollection;
    @JoinColumn(name = "id_trimmingtype", referencedColumnName = "id_trimmingtype")
    @ManyToOne(fetch = FetchType.LAZY)
    private Trimmingtype idTrimmingtype;
    @JoinColumn(name = "id_trimmingmaterial", referencedColumnName = "id_material")
    @ManyToOne(fetch = FetchType.LAZY)
    private Material idTrimmingmaterial;
    @JoinColumn(name = "id_material", referencedColumnName = "id_material")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Material idMaterial;

    public Part() {
    }

    public Part(BigDecimal idPart) {
        this.idPart = idPart;
    }

    public Part(BigDecimal idPart, String name, String code, BigDecimal weight, BigInteger height, BigInteger width, BigInteger depth, BigInteger isimplement) {
        this.idPart = idPart;
        this.name = name;
        this.code = code;
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.depth = depth;
        this.isimplement = isimplement;
    }

    public BigDecimal getIdPart() {
        return idPart;
    }

    public void setIdPart(BigDecimal idPart) {
        this.idPart = idPart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigInteger getHeight() {
        return height;
    }

    public void setHeight(BigInteger height) {
        this.height = height;
    }

    public BigInteger getWidth() {
        return width;
    }

    public void setWidth(BigInteger width) {
        this.width = width;
    }

    public BigInteger getDepth() {
        return depth;
    }

    public void setDepth(BigInteger depth) {
        this.depth = depth;
    }

    public BigInteger getIsimplement() {
        return isimplement;
    }

    public void setIsimplement(BigInteger isimplement) {
        this.isimplement = isimplement;
    }

    @XmlTransient
    public Collection<Wrappingpart> getWrappingpartCollection() {
        return wrappingpartCollection;
    }

    public void setWrappingpartCollection(Collection<Wrappingpart> wrappingpartCollection) {
        this.wrappingpartCollection = wrappingpartCollection;
    }

    @XmlTransient
    public Collection<Elementlinkage> getElementlinkageCollection() {
        return elementlinkageCollection;
    }

    public void setElementlinkageCollection(Collection<Elementlinkage> elementlinkageCollection) {
        this.elementlinkageCollection = elementlinkageCollection;
    }

    public Trimmingtype getIdTrimmingtype() {
        return idTrimmingtype;
    }

    public void setIdTrimmingtype(Trimmingtype idTrimmingtype) {
        this.idTrimmingtype = idTrimmingtype;
    }

    public Material getIdTrimmingmaterial() {
        return idTrimmingmaterial;
    }

    public void setIdTrimmingmaterial(Material idTrimmingmaterial) {
        this.idTrimmingmaterial = idTrimmingmaterial;
    }

    public Material getIdMaterial() {
        return idMaterial;
    }

    public void setIdMaterial(Material idMaterial) {
        this.idMaterial = idMaterial;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPart != null ? idPart.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Part)) {
            return false;
        }
        Part other = (Part) object;
        if ((this.idPart == null && other.idPart != null) || (this.idPart != null && !this.idPart.equals(other.idPart))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Part[ idPart=" + idPart + " ]" + " Code=" + code + " ]";
    }
}
