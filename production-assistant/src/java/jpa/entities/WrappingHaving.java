/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author mitya
 */
public interface WrappingHaving {

    public HashMap<Part, BigInteger> getChildParts();

    public HashMap<Part, BigInteger> getChildWrappingParts();

    public Collection<Wrapping> getWrappingCollection();

}
