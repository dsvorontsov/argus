/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator(name = "idElementLinkage_seq", sequenceName = "Id_ElementLinkage_seq", allocationSize = 1)
@Table(name = "elementlinkage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Elementlinkage.findAll", query = "SELECT e FROM Elementlinkage e"),
    @NamedQuery(name = "Elementlinkage.findByIdElementlinkage", query = "SELECT e FROM Elementlinkage e WHERE e.idElementlinkage = :idElementlinkage"),
    @NamedQuery(name = "Elementlinkage.findByNomination", query = "SELECT e FROM Elementlinkage e WHERE e.nomination = :nomination")})
public class Elementlinkage implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idElementLinkage_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_elementlinkage")
    private BigInteger idElementlinkage;
    @Size(max = 2147483647)
    @Column(name = "nomination")
    private String nomination;
    @Column(name = "countelement")
    private BigInteger countElement;
    @JoinColumn(name = "id_part", referencedColumnName = "id_part")
    @ManyToOne(fetch = FetchType.LAZY)
    private Part idPart;
    @JoinColumn(name = "id_linkage", referencedColumnName = "id_linkage")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Linkage idLinkage;
    @JoinColumn(name = "id_sublinkage", referencedColumnName = "id_linkage")
    @ManyToOne(fetch = FetchType.LAZY)
    private Linkage idSublinkage;

    public Elementlinkage() {
    }

    public Elementlinkage(BigInteger idElementlinkage) {
        this.idElementlinkage = idElementlinkage;
    }

    public BigInteger getIdElementlinkage() {
        return idElementlinkage;
    }

    public void setIdElementlinkage(BigInteger idElementlinkage) {
        this.idElementlinkage = idElementlinkage;
    }

    public BigInteger getCountElement() {
        return countElement;
    }

    public void setCountElement(BigInteger countElement) {
        this.countElement = countElement;
    }

    public String getNomination() {
        return nomination;
    }

    public void setNomination(String nomination) {
        this.nomination = nomination;
    }

    public Part getIdPart() {
        return idPart;
    }

    public void setIdPart(Part idPart) {
        this.idPart = idPart;
    }

    public Linkage getIdLinkage() {
        return idLinkage;
    }

    public void setIdLinkage(Linkage idLinkage) {
        this.idLinkage = idLinkage;
    }

    public Linkage getIdSublinkage() {
        return idSublinkage;
    }

    public void setIdSublinkage(Linkage idSublinkage) {
        this.idSublinkage = idSublinkage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idElementlinkage != null ? idElementlinkage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Elementlinkage)) {
            return false;
        }
        Elementlinkage other = (Elementlinkage) object;
        if ((this.idElementlinkage == null && other.idElementlinkage != null) || (this.idElementlinkage != null && !this.idElementlinkage.equals(other.idElementlinkage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Elementlinkage[ idElementlinkage=" + idElementlinkage + " ]";
    }
}
