/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator(name = "idWrappingPart_seq", sequenceName = "Id_WrappingPart_seq", allocationSize = 1)
@Table(name = "wrappingpart")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wrappingpart.findAll", query = "SELECT w FROM Wrappingpart w"),
    @NamedQuery(name = "Wrappingpart.findByIdWrappingpart", query = "SELECT w FROM Wrappingpart w WHERE w.idWrappingpart = :idWrappingpart")})
public class Wrappingpart implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idWrapping_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_wrappingpart")
    private BigInteger idWrappingpart;
    @Column(name = "countelement")
    private BigInteger countElement;
    @JoinColumn(name = "id_wrapping", referencedColumnName = "id_wrapping")
    @ManyToOne(fetch = FetchType.LAZY)
    private Wrapping idWrapping;
    @JoinColumn(name = "id_part", referencedColumnName = "id_part")
    @ManyToOne(fetch = FetchType.LAZY)
    private Part idPart;

    public Wrappingpart() {
    }

    public Wrappingpart(BigInteger idWrappingpart) {
        this.idWrappingpart = idWrappingpart;
    }

    public BigInteger getIdWrappingpart() {
        return idWrappingpart;
    }

    public void setIdWrappingpart(BigInteger idWrappingpart) {
        this.idWrappingpart = idWrappingpart;
    }

    public Wrapping getIdWrapping() {
        return idWrapping;
    }

    public BigInteger getCountElement() {
        return countElement;
    }

    public void setCountElement(BigInteger countElement) {
        this.countElement = countElement;
    }

    public void setIdWrapping(Wrapping idWrapping) {
        this.idWrapping = idWrapping;
    }

    public Part getIdPart() {
        return idPart;
    }

    public void setIdPart(Part idPart) {
        this.idPart = idPart;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idWrappingpart != null ? idWrappingpart.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wrappingpart)) {
            return false;
        }
        Wrappingpart other = (Wrappingpart) object;
        if ((this.idWrappingpart == null && other.idWrappingpart != null) || (this.idWrappingpart != null && !this.idWrappingpart.equals(other.idWrappingpart))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Wrappingpart[ idWrappingpart=" + idWrappingpart + " ]";
    }
}
