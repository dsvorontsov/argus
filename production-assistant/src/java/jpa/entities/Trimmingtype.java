/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@SequenceGenerator (name="idTrimmingtype_seq", sequenceName="Id_TrimmingType_seq", allocationSize=1)
@Table(name = "trimmingtype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trimmingtype.findAll", query = "SELECT t FROM Trimmingtype t"),
    @NamedQuery(name = "Trimmingtype.findByIdTrimmingtype", query = "SELECT t FROM Trimmingtype t WHERE t.idTrimmingtype = :idTrimmingtype"),
    @NamedQuery(name = "Trimmingtype.findByName", query = "SELECT t FROM Trimmingtype t WHERE t.name = :name"),
    @NamedQuery(name = "Trimmingtype.findByCode", query = "SELECT t FROM Trimmingtype t WHERE t.code = :code")})
public class Trimmingtype implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="idTrimmingtype_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_trimmingtype")
    private BigDecimal idTrimmingtype;
    @Size(max = 2147483647)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "code")
    private String code;
    @OneToMany(mappedBy = "idTrimmingtype", fetch = FetchType.LAZY)
    private Collection<Part> partCollection;

    public Trimmingtype() {
    }

    public Trimmingtype(BigDecimal idTrimmingtype) {
        this.idTrimmingtype = idTrimmingtype;
    }

    public Trimmingtype(BigDecimal idTrimmingtype, String code) {
        this.idTrimmingtype = idTrimmingtype;
        this.code = code;
    }

    public BigDecimal getIdTrimmingtype() {
        return idTrimmingtype;
    }

    public void setIdTrimmingtype(BigDecimal idTrimmingtype) {
        this.idTrimmingtype = idTrimmingtype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public Collection<Part> getPartCollection() {
        return partCollection;
    }

    public void setPartCollection(Collection<Part> partCollection) {
        this.partCollection = partCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTrimmingtype != null ? idTrimmingtype.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trimmingtype)) {
            return false;
        }
        Trimmingtype other = (Trimmingtype) object;
        if ((this.idTrimmingtype == null && other.idTrimmingtype != null) || (this.idTrimmingtype != null && !this.idTrimmingtype.equals(other.idTrimmingtype))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Trimmingtype[ idTrimmingtype=" + idTrimmingtype + " ]";
    }
    
}
