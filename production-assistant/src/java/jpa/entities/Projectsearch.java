/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mitya
 */
@Entity
@Table(name = "projectsearch")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projectsearch.findAll", query = "SELECT p FROM Projectsearch p"),
    @NamedQuery(name = "Projectsearch.findByCriteria", query = "SELECT m "
            + "  FROM Projectsearch m "
            + " WHERE (m.idProject  = cast (:idProject numeric(12))"
            + "    or  :idProject   is null)"
            + "   and (m.idExecutor = cast (:idExecutor numeric(12))"
            + "    or  :idExecutor  is null)")})
public class Projectsearch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id_project")
    @Id
    private BigInteger idProject;
    @Column(name = "id_executor")
    private BigInteger idExecutor;
    @Size(max = 2147483647)
    @Column(name = "fioexecutor")
    private String fioexecutor;
    @Size(max = 2147483647)
    @Column(name = "projectstatecode")
    private String projectstatecode;
    @Size(max = 2147483647)
    @Column(name = "projectstatename")
    private String projectstatename;
    @Column(name = "dbegin")
    @Temporal(TemporalType.DATE)
    private Date dbegin;
    @Column(name = "dend")
    @Temporal(TemporalType.DATE)
    private Date dend;
    @Size(max = 2147483647)
    @Column(name = "comment")
    private String comment;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    @JoinColumn(name="Id_Project")
    private List <Linkage> linkage;
//    @ManyToOne
//    @JoinColumn(name = "project_id")
//    private Project project;

    public List<Linkage> getLinkage() {
        return linkage;
    }

    public Projectsearch() {
    }

    public BigInteger getIdProject() {
        return idProject;
    }

    public BigInteger getIdExecutor() {
        return idExecutor;
    }

    public String getFioexecutor() {
        return fioexecutor;
    }

    public String getProjectstatecode() {
        return projectstatecode;
    }

    public String getProjectstatename() {
        return projectstatename;
    }

    public Date getDbegin() {
        return dbegin;
    }

    public Date getDend() {
        return dend;
    }

    public String getComment() {
        return comment;
    }
}
