/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mitya
 */
@Entity
@Table(name = "projectstate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projectstate.findAll", query = "SELECT p FROM Projectstate p"),
    @NamedQuery(name = "Projectstate.findByIdProjectstate", query = "SELECT p FROM Projectstate p WHERE p.idProjectstate = :idProjectstate"),
    @NamedQuery(name = "Projectstate.findByName", query = "SELECT p FROM Projectstate p WHERE p.name = :name"),
    @NamedQuery(name = "Projectstate.findByCode", query = "SELECT p FROM Projectstate p WHERE p.code = :code")})
public class Projectstate implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_projectstate")
    private BigDecimal idProjectstate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "code")
    private String code;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProjectstate", fetch = FetchType.LAZY)
    private Collection<Project> projectCollection;

    public Projectstate() {
    }

    public Projectstate(BigDecimal idProjectstate) {
        this.idProjectstate = idProjectstate;
    }

    public Projectstate(BigDecimal idProjectstate, String name, String code) {
        this.idProjectstate = idProjectstate;
        this.name = name;
        this.code = code;
    }

    public BigDecimal getIdProjectstate() {
        return idProjectstate;
    }

    public void setIdProjectstate(BigDecimal idProjectstate) {
        this.idProjectstate = idProjectstate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public Collection<Project> getProjectCollection() {
        return projectCollection;
    }

    public void setProjectCollection(Collection<Project> projectCollection) {
        this.projectCollection = projectCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProjectstate != null ? idProjectstate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projectstate)) {
            return false;
        }
        Projectstate other = (Projectstate) object;
        if ((this.idProjectstate == null && other.idProjectstate != null) || (this.idProjectstate != null && !this.idProjectstate.equals(other.idProjectstate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.Projectstate[ idProjectstate=" + idProjectstate + " ]";
    }
    
}
